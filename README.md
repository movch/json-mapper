# JSON Mapper

![](https://cloud.githubusercontent.com/assets/6954170/20239542/80e39f16-a913-11e6-82ee-ef085ee45dea.png)

JSON Mapper is a simple macOS app to convert JSON data to [Object Mapper](https://github.com/Hearst-DD/ObjectMapper) models. Currently it supports only Swift language, but we have plans for other languages as well.

JSON is a popular data format for information exchange between client and server. Apple provides `JSONSerialization` class as a part of Foundation framework that allows to work with JSON data without any external libraries or tools. The basic approach is described in [Working with JSON in Swift](https://developer.apple.com/swift/blog/?id=37) official article. While it absolutely workable, you may want the solution that easy to integrate with other popular libraries, such as [Alamofire](https://github.com/Alamofire/Alamofire), and that is less verbose in general. Libraries like [Alamofire ObjectMapper](https://github.com/Hearst-DD/ObjectMapper) can help there, but you still have to map JSON data fields to your classes manually. It can be really frustrating to map huge structures, and there is non-zero probability that you will make a mistake in field naming. JSON mapper is a tool that recursively iterates through all fields of provided JSON structure and prints the ready-to-use classes, that can be just copy-pasted to your application code.

## Installation

Download the latest build from the [Releases page](https://github.com/movch/json-mapper/releases), unzip it, and place to the Applications directory.

## Usage

1. Open the app;
1. Paste JSON data, you need to map, to the left input area;
1. Choose appropriate object template from dropdown on the upper right corner of the window;
1. Enter class name;
1. Copy and paste the result to Xcode for futher usage.

## Building from source

Clone the project to your computer:

    git clone git@github.com:movch/json-mapper.git

Navigate to it and install the required dependencies via CocoaPods:

    cd json-mapper
    pod install

Open `JSONMapper.xcworkspace` with Xcode and run project.

### Build requirments

* Xcode >8.1
* CocoaPods >1.1.0

## Dependencies

Several open source libraries have been used for this project:

* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON), MIT License
* [Stencil](https://github.com/kylef/Stencil), BSD license
* [Fragaria](https://github.com/mugginsoft/Fragaria), Apache License

## Authors

**Mantainer**: Michail Ovchinnikov  
**Contributors**:

* Andrey Romanov

### Artwork

* Rubber icon by [Freepik](http://www.freepik.com) from [Flaticon](http://www.flaticon.com) is licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/)

## Inspiration

We have used cool [SwiftyJSONAccelerator](https://github.com/insanoid/SwiftyJSONAccelerator), but we found it not very convinient to adapt for our needs, so it was decided to develop and mantain our own tool.

## License

Project is distributed under MIT License.
