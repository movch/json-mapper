//
//  StringExtension.swift
//  json example
//
//  Created by User on 13/10/16.
//  Copyright © 2016 a. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }

    func lowerCaseFirstLetter() -> String {
        let first = String(characters.prefix(1)).lowercased()
        let other = String(characters.dropFirst())
        return first + other
    }

    func toCamelCase() -> String {
        let specialSymbols = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
        let components = self.components(separatedBy: specialSymbols)

        if components.count == 1 {
            return components[0].lowerCaseFirstLetter()
        }

        return components
            .enumerated()
            .map { (index, element) in
                if index == 0 {
                    return element.lowercased()
                } else {
                    return element.lowercased().capitalizingFirstLetter()
                }
            }
            .joined(separator: "")
    }

    func makeResponseClassName(_ className: String) -> String {
        return "\(className.capitalizingFirstLetter())Response"
    }

    func chomp() -> String {
       return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
